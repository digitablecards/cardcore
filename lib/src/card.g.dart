// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Card _$_$_CardFromJson(Map<String, dynamic> json) {
  return _$_Card(
    json['version'] as int,
    json['id'] as int,
    json['name'] as String,
    description: json['description'] as String,
    properties: (json['properties'] as List)
        ?.map((e) =>
            e == null ? null : CardProperty.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_CardToJson(_$_Card instance) => <String, dynamic>{
      'version': instance.version,
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'properties': instance.properties,
    };
