import 'package:freezed_annotation/freezed_annotation.dart';

import 'tag.dart';

part 'card_property.freezed.dart';

part 'card_property.g.dart';

@freezed
abstract class CardProperty with _$CardProperty {
  static const defaultVersion = 0;

  static const _integerType = 'integer';
  static const _booleanType = 'boolean';
  static const _stringType = 'string';

  static const _unsupportedType = 'unsupported';

  static CardPropertyType parseType(String rawString) {
    switch (rawString) {
      case _integerType:
        return CardPropertyType.integer;
      case _booleanType:
        return CardPropertyType.boolean;
      case _stringType:
        return CardPropertyType.string;
    }
    return CardPropertyType.unsupported;
  }

  static dynamic parseValue(CardPropertyType type, String value) {
    try {
      switch (type) {
        case CardPropertyType.integer:
          return int.parse(value);
        case CardPropertyType.boolean:
          return _parseBool(value);
        case CardPropertyType.string:
          return value;
        default:
          return value;
      }
    } on FormatException catch (_) {
      throw FormatException(
          'Wrong format value: expected $type, but value is $value');
    }
  }

  const factory CardProperty(
    @JsonKey(name: 'version') int version,
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name') String name,
    @JsonKey(name: 'type') CardPropertyType type,
    @JsonKey(name: 'value') dynamic value, {
    @JsonKey(name: 'description') String description,
    @JsonKey(name: 'tags') List<Tag> tags,
  }) = _CardProperty;

  factory CardProperty.fromJson(Map<String, dynamic> json) =>
      _$CardPropertyFromJson(json);
}

enum CardPropertyType {
  @JsonValue(CardProperty._integerType)
  integer,
  @JsonValue(CardProperty._booleanType)
  boolean,
  @JsonValue(CardProperty._stringType)
  string,
  @JsonValue(CardProperty._unsupportedType)
  unsupported,
}

bool _parseBool(String value) {
  final b = value.toLowerCase();
  if (b == 'true' || b == 'false') {
    return b == 'true';
  } else {
    throw FormatException();
  }
}
