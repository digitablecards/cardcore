// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_property.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CardProperty _$_$_CardPropertyFromJson(Map<String, dynamic> json) {
  return _$_CardProperty(
    json['version'] as int,
    json['id'] as int,
    json['name'] as String,
    _$enumDecodeNullable(_$CardPropertyTypeEnumMap, json['type']),
    json['value'],
    description: json['description'] as String,
    tags: (json['tags'] as List)
        ?.map((e) => e == null ? null : Tag.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_CardPropertyToJson(_$_CardProperty instance) =>
    <String, dynamic>{
      'version': instance.version,
      'id': instance.id,
      'name': instance.name,
      'type': _$CardPropertyTypeEnumMap[instance.type],
      'value': instance.value,
      'description': instance.description,
      'tags': instance.tags,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$CardPropertyTypeEnumMap = {
  CardPropertyType.integer: 'integer',
  CardPropertyType.boolean: 'boolean',
  CardPropertyType.string: 'string',
  CardPropertyType.unsupported: 'unsupported',
};
