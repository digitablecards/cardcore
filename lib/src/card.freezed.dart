// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'card.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Card _$CardFromJson(Map<String, dynamic> json) {
  return _Card.fromJson(json);
}

class _$CardTearOff {
  const _$CardTearOff();

// ignore: unused_element
  _Card call(@JsonKey(name: 'version') int version, @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      {@JsonKey(name: 'description') String description,
      @JsonKey(name: 'properties') List<CardProperty> properties}) {
    return _Card(
      version,
      id,
      name,
      description: description,
      properties: properties,
    );
  }
}

// ignore: unused_element
const $Card = _$CardTearOff();

mixin _$Card {
  @JsonKey(name: 'version')
  int get version;
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name')
  String get name;
  @JsonKey(name: 'description')
  String get description;
  @JsonKey(name: 'properties')
  List<CardProperty> get properties;

  Map<String, dynamic> toJson();
  $CardCopyWith<Card> get copyWith;
}

abstract class $CardCopyWith<$Res> {
  factory $CardCopyWith(Card value, $Res Function(Card) then) =
      _$CardCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      @JsonKey(name: 'description') String description,
      @JsonKey(name: 'properties') List<CardProperty> properties});
}

class _$CardCopyWithImpl<$Res> implements $CardCopyWith<$Res> {
  _$CardCopyWithImpl(this._value, this._then);

  final Card _value;
  // ignore: unused_field
  final $Res Function(Card) _then;

  @override
  $Res call({
    Object version = freezed,
    Object id = freezed,
    Object name = freezed,
    Object description = freezed,
    Object properties = freezed,
  }) {
    return _then(_value.copyWith(
      version: version == freezed ? _value.version : version as int,
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
      description:
          description == freezed ? _value.description : description as String,
      properties: properties == freezed
          ? _value.properties
          : properties as List<CardProperty>,
    ));
  }
}

abstract class _$CardCopyWith<$Res> implements $CardCopyWith<$Res> {
  factory _$CardCopyWith(_Card value, $Res Function(_Card) then) =
      __$CardCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      @JsonKey(name: 'description') String description,
      @JsonKey(name: 'properties') List<CardProperty> properties});
}

class __$CardCopyWithImpl<$Res> extends _$CardCopyWithImpl<$Res>
    implements _$CardCopyWith<$Res> {
  __$CardCopyWithImpl(_Card _value, $Res Function(_Card) _then)
      : super(_value, (v) => _then(v as _Card));

  @override
  _Card get _value => super._value as _Card;

  @override
  $Res call({
    Object version = freezed,
    Object id = freezed,
    Object name = freezed,
    Object description = freezed,
    Object properties = freezed,
  }) {
    return _then(_Card(
      version == freezed ? _value.version : version as int,
      id == freezed ? _value.id : id as int,
      name == freezed ? _value.name : name as String,
      description:
          description == freezed ? _value.description : description as String,
      properties: properties == freezed
          ? _value.properties
          : properties as List<CardProperty>,
    ));
  }
}

@JsonSerializable()
class _$_Card implements _Card {
  const _$_Card(@JsonKey(name: 'version') this.version,
      @JsonKey(name: 'id') this.id, @JsonKey(name: 'name') this.name,
      {@JsonKey(name: 'description') this.description,
      @JsonKey(name: 'properties') this.properties})
      : assert(version != null),
        assert(id != null),
        assert(name != null);

  factory _$_Card.fromJson(Map<String, dynamic> json) =>
      _$_$_CardFromJson(json);

  @override
  @JsonKey(name: 'version')
  final int version;
  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name')
  final String name;
  @override
  @JsonKey(name: 'description')
  final String description;
  @override
  @JsonKey(name: 'properties')
  final List<CardProperty> properties;

  @override
  String toString() {
    return 'Card(version: $version, id: $id, name: $name, description: $description, properties: $properties)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Card &&
            (identical(other.version, version) ||
                const DeepCollectionEquality()
                    .equals(other.version, version)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.properties, properties) ||
                const DeepCollectionEquality()
                    .equals(other.properties, properties)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(version) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(properties);

  @override
  _$CardCopyWith<_Card> get copyWith =>
      __$CardCopyWithImpl<_Card>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CardToJson(this);
  }
}

abstract class _Card implements Card {
  const factory _Card(@JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name,
      {@JsonKey(name: 'description') String description,
      @JsonKey(name: 'properties') List<CardProperty> properties}) = _$_Card;

  factory _Card.fromJson(Map<String, dynamic> json) = _$_Card.fromJson;

  @override
  @JsonKey(name: 'version')
  int get version;
  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name')
  String get name;
  @override
  @JsonKey(name: 'description')
  String get description;
  @override
  @JsonKey(name: 'properties')
  List<CardProperty> get properties;
  @override
  _$CardCopyWith<_Card> get copyWith;
}
