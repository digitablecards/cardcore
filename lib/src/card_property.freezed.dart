// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'card_property.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
CardProperty _$CardPropertyFromJson(Map<String, dynamic> json) {
  return _CardProperty.fromJson(json);
}

class _$CardPropertyTearOff {
  const _$CardPropertyTearOff();

// ignore: unused_element
  _CardProperty call(
      @JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      @JsonKey(name: 'type') CardPropertyType type,
      @JsonKey(name: 'value') dynamic value,
      {@JsonKey(name: 'description') String description,
      @JsonKey(name: 'tags') List<Tag> tags}) {
    return _CardProperty(
      version,
      id,
      name,
      type,
      value,
      description: description,
      tags: tags,
    );
  }
}

// ignore: unused_element
const $CardProperty = _$CardPropertyTearOff();

mixin _$CardProperty {
  @JsonKey(name: 'version')
  int get version;
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name')
  String get name;
  @JsonKey(name: 'type')
  CardPropertyType get type;
  @JsonKey(name: 'value')
  dynamic get value;
  @JsonKey(name: 'description')
  String get description;
  @JsonKey(name: 'tags')
  List<Tag> get tags;

  Map<String, dynamic> toJson();
  $CardPropertyCopyWith<CardProperty> get copyWith;
}

abstract class $CardPropertyCopyWith<$Res> {
  factory $CardPropertyCopyWith(
          CardProperty value, $Res Function(CardProperty) then) =
      _$CardPropertyCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      @JsonKey(name: 'type') CardPropertyType type,
      @JsonKey(name: 'value') dynamic value,
      @JsonKey(name: 'description') String description,
      @JsonKey(name: 'tags') List<Tag> tags});
}

class _$CardPropertyCopyWithImpl<$Res> implements $CardPropertyCopyWith<$Res> {
  _$CardPropertyCopyWithImpl(this._value, this._then);

  final CardProperty _value;
  // ignore: unused_field
  final $Res Function(CardProperty) _then;

  @override
  $Res call({
    Object version = freezed,
    Object id = freezed,
    Object name = freezed,
    Object type = freezed,
    Object value = freezed,
    Object description = freezed,
    Object tags = freezed,
  }) {
    return _then(_value.copyWith(
      version: version == freezed ? _value.version : version as int,
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
      type: type == freezed ? _value.type : type as CardPropertyType,
      value: value == freezed ? _value.value : value as dynamic,
      description:
          description == freezed ? _value.description : description as String,
      tags: tags == freezed ? _value.tags : tags as List<Tag>,
    ));
  }
}

abstract class _$CardPropertyCopyWith<$Res>
    implements $CardPropertyCopyWith<$Res> {
  factory _$CardPropertyCopyWith(
          _CardProperty value, $Res Function(_CardProperty) then) =
      __$CardPropertyCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      @JsonKey(name: 'type') CardPropertyType type,
      @JsonKey(name: 'value') dynamic value,
      @JsonKey(name: 'description') String description,
      @JsonKey(name: 'tags') List<Tag> tags});
}

class __$CardPropertyCopyWithImpl<$Res> extends _$CardPropertyCopyWithImpl<$Res>
    implements _$CardPropertyCopyWith<$Res> {
  __$CardPropertyCopyWithImpl(
      _CardProperty _value, $Res Function(_CardProperty) _then)
      : super(_value, (v) => _then(v as _CardProperty));

  @override
  _CardProperty get _value => super._value as _CardProperty;

  @override
  $Res call({
    Object version = freezed,
    Object id = freezed,
    Object name = freezed,
    Object type = freezed,
    Object value = freezed,
    Object description = freezed,
    Object tags = freezed,
  }) {
    return _then(_CardProperty(
      version == freezed ? _value.version : version as int,
      id == freezed ? _value.id : id as int,
      name == freezed ? _value.name : name as String,
      type == freezed ? _value.type : type as CardPropertyType,
      value == freezed ? _value.value : value as dynamic,
      description:
          description == freezed ? _value.description : description as String,
      tags: tags == freezed ? _value.tags : tags as List<Tag>,
    ));
  }
}

@JsonSerializable()
class _$_CardProperty implements _CardProperty {
  const _$_CardProperty(
      @JsonKey(name: 'version') this.version,
      @JsonKey(name: 'id') this.id,
      @JsonKey(name: 'name') this.name,
      @JsonKey(name: 'type') this.type,
      @JsonKey(name: 'value') this.value,
      {@JsonKey(name: 'description') this.description,
      @JsonKey(name: 'tags') this.tags})
      : assert(version != null),
        assert(id != null),
        assert(name != null),
        assert(type != null),
        assert(value != null);

  factory _$_CardProperty.fromJson(Map<String, dynamic> json) =>
      _$_$_CardPropertyFromJson(json);

  @override
  @JsonKey(name: 'version')
  final int version;
  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name')
  final String name;
  @override
  @JsonKey(name: 'type')
  final CardPropertyType type;
  @override
  @JsonKey(name: 'value')
  final dynamic value;
  @override
  @JsonKey(name: 'description')
  final String description;
  @override
  @JsonKey(name: 'tags')
  final List<Tag> tags;

  @override
  String toString() {
    return 'CardProperty(version: $version, id: $id, name: $name, type: $type, value: $value, description: $description, tags: $tags)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CardProperty &&
            (identical(other.version, version) ||
                const DeepCollectionEquality()
                    .equals(other.version, version)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.tags, tags) ||
                const DeepCollectionEquality().equals(other.tags, tags)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(version) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(value) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(tags);

  @override
  _$CardPropertyCopyWith<_CardProperty> get copyWith =>
      __$CardPropertyCopyWithImpl<_CardProperty>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CardPropertyToJson(this);
  }
}

abstract class _CardProperty implements CardProperty {
  const factory _CardProperty(
      @JsonKey(name: 'version') int version,
      @JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name,
      @JsonKey(name: 'type') CardPropertyType type,
      @JsonKey(name: 'value') dynamic value,
      {@JsonKey(name: 'description') String description,
      @JsonKey(name: 'tags') List<Tag> tags}) = _$_CardProperty;

  factory _CardProperty.fromJson(Map<String, dynamic> json) =
      _$_CardProperty.fromJson;

  @override
  @JsonKey(name: 'version')
  int get version;
  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name')
  String get name;
  @override
  @JsonKey(name: 'type')
  CardPropertyType get type;
  @override
  @JsonKey(name: 'value')
  dynamic get value;
  @override
  @JsonKey(name: 'description')
  String get description;
  @override
  @JsonKey(name: 'tags')
  List<Tag> get tags;
  @override
  _$CardPropertyCopyWith<_CardProperty> get copyWith;
}
