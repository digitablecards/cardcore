import 'package:freezed_annotation/freezed_annotation.dart';

import 'card_property.dart';

part 'card.freezed.dart';

part 'card.g.dart';

@freezed
abstract class Card with _$Card {
  static const defaultVersion = 0;

  const factory Card(
    @JsonKey(name: 'version') int version,
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name') String name, {
    @JsonKey(name: 'description') String description,
    @JsonKey(name: 'properties') List<CardProperty> properties,
  }) = _Card;

  factory Card.fromJson(Map<String, dynamic> json) => _$CardFromJson(json);
}
